from flask import Flask,request,jsonify
from flask_restplus import Api,Resource
from pymongo import MongoClient
import logging
from logging.handlers import RotatingFileHandler


app = Flask(__name__)
api = Api(app)

connection = MongoClient('localhost',27017)
db = connection['Rest_db']


@app.route("/log")

# fetch data from the database

# fetch data from the database
def get():
    app.logger.error("error")
    return "Hello"

if __name__ == "__main__":
    # initialize the log handler
    logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)

    # set the log handler level
    logHandler.setLevel(logging.INFO)

    # set the app logger level
    app.logger.setLevel(logging.INFO)

    app.logger.addHandler(logHandler)
    app.run()