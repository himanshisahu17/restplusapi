from flask import Flask,jsonify,request
from flask_restplus import Api,Resource
from pymongo import MongoClient
import logging
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
api = Api(app)

connection = MongoClient('localhost',27017)
db = connection['Rest_db']


@api.route('/entity/')
class GetPost(Resource):

# fetch data from the database
    def get(self):
        name = request.args.get('name', None)
        try:
            col = db['entity']
            if name:

                name = request.args.get('name', None)
                q = col.find_one({'name': name})
                output = {}
                if q:
                    output = {'name': q['name'], 'description': q['description']}
                return jsonify({'result': output})
            else :
                output = []
                for q in col.find():
                    output.append(
                        {'name': q['name'],
                         'description': q['description']
                         }
                    )
                return jsonify({'result': output})

        except Exception as error:
                app.logger.error('"No data Found error')
                return jsonify({'Error': str(error)})


#To insert data in database
    def post(self):
        if request.method == "POST":
            name = request.values.get('name')
            description = request.values.get('description')


            try:
                col = db['entity']
                if name and description:

                    col.insert_one({'name': name, 'description': description})
                    output = {'name': name, 'description': description}
                    return jsonify({'result': output})

                else :
                    return ("NULL value not allowed  ")

            except Exception as error:
                app.logger.info("POST request not Found")
                return jsonify({'Error': str(error)})


#to update value in database
    def put(self):
        name = request.values.get('name')
        description = request.values.get('description')

        try:

            col = db['entity']
            q = col.find({"_id": name})
            if q:
                val = col.update_one({'name': name}, {'$set': {'description': description}})
            else:
                app.logger.error("Data not Found")

        except Exception as error:
            return jsonify({'Error': str(error)})
        return "Successfully data updated"


#to delete the value from database
    def delete(self):

        name = request.values.get('name')

        try:
            col = db['entity']
            if col.find_one({'name': name}) == None:
                raise Exception
            else:
                col.delete_one({'name': name})
                output = "{} deleted successfully!".format(name)

            return jsonify({'result': output})
        except Exception as e:
            app.logger.error('Name not found in DB')
            return jsonify({'Error': str(e)})


if __name__ == '__main__':

    # initialize the log handler
    logHandler = RotatingFileHandler('info.log', maxBytes=1000, backupCount=1)

    # set the log handler level
    logHandler.setLevel(logging.INFO)

    # set the app logger level
    app.logger.setLevel(logging.INFO)

    app.logger.addHandler(logHandler)

    app.run(debug=True)